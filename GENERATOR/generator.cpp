// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   generator.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: clement <lcaminon@student.42.fr>           +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/12/23 15:14:08 by clement           #+#    #+#             //
//   Updated: 2014/12/23 15:21:17 by clement          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include        <SFML/Graphics.hpp>
#include        <SFML/Window.hpp>
#include        <iostream>

int             main()
{
	sf::RenderWindow App(sf::VideoMode(800, 800, 32),"Particle");
	sf::Sprite sprite_a1;
	sf::Sprite sprite_b1;
	sf::Sprite sprite_b2;
	sf::Sprite sprite_b3;
	sf::Sprite sprite_c1;
	sf::Sprite sprite_c2;
	sf::Sprite sprite_c3;
	sf::Texture texture_a;
	sf::Texture texture_b;
	sf::Texture texture_c;
    int x = 0;
    int y = 200;
	sf::Clock clock;
	sf::Time elapsed;

    texture_a.loadFromFile("./resources/mail_a.png");
    texture_b.loadFromFile("./resources/mail_b.png");
    texture_c.loadFromFile("./resources/mail_c.png");
    sprite_a1.setTexture(texture_a);
    sprite_b1.setTexture(texture_b);
    sprite_b2.setTexture(texture_b);
    sprite_b3.setTexture(texture_b);
    sprite_c1.setTexture(texture_c);
    sprite_c2.setTexture(texture_c);
    sprite_c3.setTexture(texture_c);

	std::vector<sf::Sprite> tabSprite{sprite_a1, sprite_b1, sprite_b2, sprite_b3,
            sprite_c1, sprite_c2, sprite_c3};

    for (unsigned int i(0); i < tabSprite.size(); ++i)
        tabSprite[i].setPosition(x, y);
    int a = 255;
    App.setFramerateLimit(60);
    clock.restart();
    while (App.isOpen())
    {
        elapsed = clock.getElapsedTime();
        App.clear();
        for (unsigned int i(0); i < tabSprite.size(); ++i)
        {
            int moveY = rand() % (5);
            int moveX = rand() % (10);
            tabSprite[i].setColor(sf::Color(255, 255, 255, a));
            App.draw(tabSprite[i]);
            tabSprite[i].move(moveX, moveY);
        }
        if (elapsed.asSeconds() > 1)
            a = a - 5;
		if (a <= 0)
            a = 0;
        App.display();
    }
}
