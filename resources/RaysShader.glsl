// ----------------------------------------- Header ------------------------------------------
#define float2 vec2
#define float3 vec3
#define float4 vec4
#define float3x3 mat3
#define float4x4 mat4
#ifdef COMPILING_VS
	#define OUTIN out 
#else 
	#define OUTIN in 
#endif 



// ----------------------------------- Per Frame --------------------------------------
uniform float time;

uniform float4x4 viewI;


// --------------------------------------- Per Object -----------------------------------------
uniform float4x4 world;

uniform float SpeedU = 0.015;

uniform float SpeedV = 0.0;

uniform float CenterX = 0.7;

uniform float CenterY = 0.9;

uniform float Speed = 0.1;

uniform float4x4 wvp;


// ---------------------------------------- Textures -----------------------------------------
uniform sampler2D MaskA;



uniform sampler2D MaskB;



uniform sampler2D cloudA;




// -------------------------------------- APP and DATA  --------------------------------------
#if defined(COMPILING_VS)
	in float3 IN_Position;
	in float3 IN_Normal;
	in float2 IN_map1;
#endif 

	OUTIN float4 OUT_Position;
	OUTIN float4 OUT_Normal;
	OUTIN float4 OUT_map1;
	OUTIN float4 OUT_WorldPosition;

// -------------------------------------- UVPannerFunction --------------------------------------
struct UVPannerOutput
{
	float2 PannedUV;
};

UVPannerOutput UVPannerFunction(float2 UV, float Time)
{
	UVPannerOutput OUT;

	float MulOp = (Time * SpeedU);
	float MulOp1310 = (Time * SpeedV);
	float2 VectorConstruct = float2(MulOp, MulOp1310);
	float2 AddOp = (UV + VectorConstruct.xy);
	OUT.PannedUV = AddOp;

	return OUT;
}

// -------------------------------------- UVRotatorFunction --------------------------------------
struct UVRotatorOutput
{
	float2 RotatedUV;
};

UVRotatorOutput UVRotatorFunction(float2 UV, float Time)
{
	UVRotatorOutput OUT;

	float2 Center = float2(CenterX, CenterY);
	float2 SubOp = (UV - Center.xy);
	float MulOp = (Time * Speed);
	float CosOp = cos(MulOp);
	float SinOp = sin(MulOp);
	float NegateOp = -(SinOp);
	float2 VectorConstruct = float2(CosOp, NegateOp);
	float DotOp = dot(SubOp, VectorConstruct.xy);
	float2 VectorConstruct1915 = float2(SinOp, CosOp);
	float DotOp1916 = dot(SubOp, VectorConstruct1915.xy);
	float2 VectorConstruct1917 = float2(DotOp, DotOp1916);
	float2 AddOp = (Center.xy + VectorConstruct1917.xy);
	OUT.RotatedUV = AddOp;

	return OUT;
}

// -------------------------------------- DesaturateColorFunction --------------------------------------
struct DesaturateColorOutput
{
	float DesaturateColor;
};

DesaturateColorOutput DesaturateColorFunction(float3 Color)
{
	DesaturateColorOutput OUT;

	float3 Col = float3(0.300008,0.6,0.100008);
	float DotOp = dot(clamp(Color, 0.0, 1.0), Col.xyz);
	OUT.DesaturateColor = DotOp;

	return OUT;
}

// -------------------------------------- ShaderVertex --------------------------------------
#if defined(COMPILING_VS)
	void main(void)
	{
		OUT_Position = float4(IN_Position, 1);
		float3 MulOp = ((float3x3(world)) * IN_Normal);
		float3 NormalN = normalize(MulOp);
		float4 WorldNormal = float4(NormalN.x, NormalN.y, NormalN.z, 1.0);
		OUT_Normal = WorldNormal;
		float4 OutUVs = float4(IN_map1.x, IN_map1.y, 0.0, 0.0);
		OUT_map1 = OutUVs;
		float4 WorldPos = (world * OUT_Position);
		OUT_WorldPosition = WorldPos;
		float4 WVSpace = (wvp * OUT_Position);
		OUT_Position = WVSpace;
	}
#endif 

// -------------------------------------- ShaderPixel --------------------------------------
#if defined(COMPILING_PS)
	out float4 OUT_Color;
#endif 

#if defined(COMPILING_PS)
	void main(void)
	{
		float InvertSatMask = (1.0 - clamp(0.0, 0.0, 1.0));
		float3 NoAlbedo = float3(0.0,0.0,0.0);
		float3 ReplaceDiffuseWithReflection = (InvertSatMask * NoAlbedo.xyz);
		float3 NormOp = normalize(OUT_Normal.xyz);
		float3 FlippedNormals = mix(-(NormOp), NormOp, float(gl_FrontFacing));
		UVPannerOutput UVPanner1305 = UVPannerFunction(OUT_map1.xy, time);
		float4 Sampler = texture(MaskA, UVPanner1305.PannedUV);
		UVRotatorOutput UVRotator1904 = UVRotatorFunction(OUT_map1.xy, time);
		float4 Sampler1199 = texture(MaskB, UVRotator1904.RotatedUV);
		float4 Sampler2226 = texture(cloudA, OUT_map1.xy);
		float MulOp = ((Sampler.w * Sampler1199.xyz.x) * Sampler2226.xyz.x);
		float ClampOpacity = clamp(MulOp, 0.0, 1.0);
		float3 CameraPosition = float3(viewI[0].w, viewI[1].w, viewI[2].w);
		float3 CamVec = (CameraPosition - OUT_WorldPosition.xyz);
		float3 CamVecNorm = normalize(CamVec);
		float4 LightLoopTotal11 = float4(0,0,0,0);
		float3 NoReflection = float3(0.0,0.0,0.0);
		float3 ReflectXmask = (0.0 * NoReflection.xyz);
		float3 DefaultIBLColor = float3(0.0,0.0,0.0);
		float3 PreMultipliedAlpha = ((Sampler.xyz + DefaultIBLColor.xyz) * ClampOpacity);
		float3 AddReflection = (ReflectXmask + PreMultipliedAlpha);
		DesaturateColorOutput DesaturateColor314 = DesaturateColorFunction(ReflectXmask);
		float OpacityAndReflection = (ClampOpacity + DesaturateColor314.DesaturateColor);
		float4 TotalAmbientAndOpacity = float4(AddReflection.x, AddReflection.y, AddReflection.z, OpacityAndReflection);
		float4 LightLoopAndAfterLoop = (LightLoopTotal11 + TotalAmbientAndOpacity);
		OUT_Color = LightLoopAndAfterLoop;
	}
#endif 

