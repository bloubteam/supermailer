// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ContactListener.cpp                                :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/08/23 20:33:45 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/23 22:03:15 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <ContactListener.h>

void ContactListener::BeginContact(b2Contact* contact)
{
	void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
	if (bodyUserData)
        static_cast<IElement*>(bodyUserData)->beginContact(contact->GetFixtureB()->GetBody()->GetUserData());
	bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
	if (bodyUserData)
        static_cast<IElement*>(bodyUserData)->beginContact(contact->GetFixtureA()->GetBody()->GetUserData());
}

void ContactListener::EndContact(b2Contact* contact)
{
	void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
	if (bodyUserData)
        static_cast<IElement*>(bodyUserData)->endContact(contact->GetFixtureB()->GetBody()->GetUserData());
	bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
	if (bodyUserData)
        static_cast<IElement*>(bodyUserData)->endContact(contact->GetFixtureA()->GetBody()->GetUserData());
}
