// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Shark.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/08/24 16:50:26 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/29 18:46:38 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Shark.h>

std::shared_ptr<Shark> Shark::make(b2World& world, b2Vec2 pos)
{
	b2BodyDef bodyDef;
	bodyDef.position = pos;
	bodyDef.type = b2_staticBody;
	b2Body* body = world.CreateBody(&bodyDef);

	b2CircleShape shape;
	shape.m_radius = (100/2) / SCALE;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	body->CreateFixture(&fixtureDef);
	std::shared_ptr<Shark> shark = std::make_shared<Shark>(body);
	body->SetUserData(shark.get());

	return (shark);
}

Shark::Shark(b2Body *oself) : self(oself)
{
	Ressources *ressources = Ressources::getInstance();
	_sprite = std::make_shared<Sprite>(ressources->getTexture("fish_a"), 1, 300);

	// _rectal.setRadius(50);
	// _rectal.setOutlineThickness(-1);
	// _rectal.setFillColor(sf::Color::Transparent);
	// _rectal.setOutlineColor(sf::Color::Red);
	// _rectal.setPosition(100, 100);
}

bool Shark::dead()
{
	return (false);
}

void Shark::remove(b2World &world)
{
	world.DestroyBody(self);
}

void Shark::update(b2World &world, float timeStep)
{
	(void)world;
	_sprite->update(timeStep);
	b2Vec2 vect = self->GetPosition();
	setPosition(vect.x * SCALE - _sprite->getSize() / 2, -vect.y * SCALE - _sprite->getSize() / 2);
}

ElementType Shark::getType()
{
	return (ELM_OBSTACLE);
}

void Shark::beginContact(void *obj)
{
	(void)obj;
}

void Shark::endContact(void *obj)
{
	(void)obj;
}

void Shark::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(*_sprite, states);
	// target.draw(_rectal, states);
}
