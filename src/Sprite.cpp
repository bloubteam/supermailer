// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Sprite.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cplumas <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/08/23 23:19:18 by cplumas           #+#    #+#             //
//   Updated: 2014/08/24 05:27:46 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include				"Sprite.hh"

Sprite::Sprite(sf::Texture *texture, float imgPSec, int size) : _limit(1/imgPSec), _size(size)
{
	_anim.x = 0;
	_anim.y = 0;
	_time = 0;
	_sprite.setTexture(*texture);
	_sprite.setTextureRect(sf::IntRect(_anim.x * _size, _anim.y * _size, _size, _size));
}

void	Sprite::nextSprite()
{
	_anim.x += 1;
	if (_anim.x * _size >= (int)_sprite.getTexture()->getSize().x)
		_anim.x = 0;
	_sprite.setTextureRect(sf::IntRect(_anim.x * _size, _anim.y * _size, _size, _size)); // probablement remplacer size par un vecteur, plus tard ...
}

void	Sprite::update(float timeStep)
{
	_time += timeStep;
	if (_time > _limit)
	{
		_time -= _limit;
		nextSprite();
	}
}

void Sprite::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

    target.draw(_sprite, states);
}

float Sprite::getSize()
{
	return (_size);
}
