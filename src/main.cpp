// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/20 22:09:11 by lcaminon          #+#    #+#             //
//   Updated: 2014/09/04 00:03:49 by lcaminon         ###   ########.fr       //
//   Updated: 2014/09/01 19:41:48 by cplumas          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include	<Game.h>
#include	<Menu.h>
#include	<Intro.h>

int			main()
{
    Ressources   *ressources = Ressources::getInstance();

	try
	{
		std::cout << "Box2D v" << b2_version.major << "." << b2_version.minor << "." << b2_version.revision << std::endl;
        sf::RenderWindow window(*(sf::VideoMode::getFullscreenModes().begin()), "SuperMailer");//, sf::Style::Fullscreen);
		window.setFramerateLimit(60);
        ressources->load(&window);
		{
			 Intro intro;

			 intro.main();
		}
		while (ressources->isLoading())
			sf::sleep(sf::seconds(1/60.));
        if (!ressources->isOk())
            throw;

		//encapsulation de Game
		{
			Menu	Menu;
			Game    game;

			while (window.isOpen())
			{
				if (ressources->getStatut() == STA_MENU)
					Menu.main();
				else if (ressources->getStatut() == STA_GAME)
					game.main();
			}
		}
		ressources->kill();
        return (0);
    }
	catch (...)
	{
		std::cout << "Sorry, an unhandled exeption occured :(" << std::endl;
		return (-1);
	}
}
