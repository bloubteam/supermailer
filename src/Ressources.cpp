//
// Ressources.cpp for similis in /home/lcaminon/Documents/projets/similis
//
// Made by Loic Caminondo
// Email   <loic@caminondo.fr>
//
// Started on  Tue Sep 17 15:08:04 2013 Loic Caminondo
// Last update Sun Nov 17 02:54:38 2013 Loic Caminondo
//

#include		"Ressources.hh"

Ressources::Ressources()
	: _th_loading(&Ressources::loading, this)
{
  _isLoading = false;
  _loadingError = false;
}

Ressources::~Ressources()
{
}

void			Ressources::loading()
{
	// charger les ressources inutiles a l'intro

	std::cout << "Loading starting." << std::endl;

	_statut = STA_MENU;

	_rGenerator.seed(std::chrono::system_clock::now().time_since_epoch().count());

	std::map<std::string, std::string> textures;
	textures["wave_a"] = "wave_a.png";
	textures["wave_b"] = "wave_b.png";
	textures["wave_c"] = "wave_c.png";
	textures["run"] = "run.png";
	textures["jump"] = "jump.png";
	textures["fall"] = "jump.png";
	textures["backg"] = "backg.png";
	textures["backw"] = "back_wave.png";
	textures["back_isle_a"] = "back_isle_a.png";
	textures["back_isle_b"] = "back_isle_b.png";
	textures["degrade"] = "degrade.png";
	textures["sky"] = "sky.png";
	textures["isle"] = "isle.png";
	textures["fish_a"] = "fish_a.png";
	textures["box_a"] = "box_a.png";
	textures["ship_a"] = "ship_a.png";	
	textures["highscore"] = "highscore.png";
	textures["gameover"] = "gameoverscreen.png";

	std::map<std::string, std::string> sounds;
	sounds["jump"] = "vomi.ogg";
	sounds["dead"] = "vomi.ogg";

	std::map<std::string, std::string> musics;
	musics["main"] = "backinblack.ogg";

	try
	{
		int loaded = 0;
		int total = textures.size() + sounds.size() + musics.size();
		std::cout << "Textures ...." << std::endl;
		for (auto &it : textures)
		{
			std::cout << "\t" << it.first << " : " << it.second << std::endl;
			if (!_texture[it.first].loadFromFile(_res_dir + it.second))
				throw;
			_texture[it.first].setSmooth(true);
			_texture[it.first].setRepeated(true);
			std::cout << "\t\t=> OK (" << ++loaded*100/total << "%)" << std::endl;
		}

		std::cout << "Sounds ...." << std::endl;
		for (auto &it : sounds)
		{
			std::cout << "\t" << it.first << " : " << it.second << std::endl;
			if (!_sounds[it.first].loadFromFile(_res_dir + it.second))
				throw;
			std::cout << "\t\t=> OK (" << ++loaded*100/total << "%)" << std::endl;
		}

		std::cout << "Musics ...." << std::endl;
		for (auto &it : musics)
		{
			std::cout << "\t" << it.first << " : " << it.second << std::endl;
			if (!_musics[it.first].openFromFile(_res_dir + it.second))
				throw;
			std::cout << "\t\t=> OK (" << ++loaded*100/total << "%)" << std::endl;
		}
	}
	catch (...)
	{
		std::cerr << "Error when loading ..." << std::endl;
		_loadingError = true;
	}

	_isLoading = false;
	std::cout << "Loading finished" << std::endl;
}

void			Ressources::load(sf:: RenderWindow *window)
{
	// ici, charger les ressources utilise pour l'intro

	std::cout << "Preload starting." << std::endl;

	_isLoading = true;

	_window = window;

	_language = LAN_FR;

	_res_dir = "resources/";

	std::map<Language, std::string> files;
	files[LAN_FR] = "FR";
	files[LAN_EN] = "EN";
	files[LAN_EO] = "EO";

	for (auto &it : files)
	{
		std::ifstream file(_res_dir + it.second);
		std::string key;
		std::string str;

		if (file)
		{
			file >> key;
			if (getline(file, str))
			{
				_texts[it.first][key] = str;
				size_t startpos = str.find_first_not_of(" \t");
				if(std::string::npos != startpos)
				{
					str = str.substr(startpos);
				}
			}
		}
	}
	_texts[LAN_FR]["new"] = "Nouvelle Partie";

	if (!_font.loadFromFile(_res_dir + "bubble.ttf"))
		_loadingError = true;

	std::map<std::string, std::string> textures;
	textures["default"] = "box_a.png";
	textures["intro_a"] = "gameoverscreen.png";
	textures["intro_b"] = "gameoverscreen.png";

	for (auto &it : textures)
	{
		if (!_texture[it.first].loadFromFile(_res_dir + it.second))
			throw;
	}

	std::cout << "Preload finished." << std::endl;

	_th_loading.launch();
}

bool			Ressources::isLoading()
{
  return (_isLoading);
}

bool			Ressources::isOk()
{
  return (!_loadingError);
}

sf::Font		*Ressources::getFont(std::string type)
{
  (void)type;
  return (&_font);
}

sf::Texture		*Ressources::getTexture(std::string type)
{
	auto it = _texture.find(type);
	if (it != _texture.end())
		return (&(it->second));
	throw;
	return (&_texture["default"]);
}

sf::RenderWindow	*Ressources::getWindow()
{
  return (_window);
}

sf::Music *Ressources::getMusic(std::string type)
{
	return (&_musics[type]);
}

sf::SoundBuffer *Ressources::getSound(std::string type)
{
	return (&_sounds[type]);
}

std::string		Ressources::getText(std::string text)
{
	auto it = _texts[_language].find(text);
	if (it != _texts[_language].end())
		return (it->second);
    return (text);
}

int			Ressources::getDef()
{
  if (_quality == QUA_HIG)
    return (360);
  else if (_quality == QUA_MED)
    return (72);
  else
    return (12);
}

State             Ressources::getStatut()
{
    return (this->_statut);
}

void            Ressources::setStatut(State statut)
{
    this->_statut = statut;
}

std::default_random_engine *Ressources::getREngine()
{
	return (&_rGenerator);
}
