// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Shark.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/08/24 16:50:26 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/29 19:25:08 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Ship.h>

std::shared_ptr<Ship> Ship::make(b2World& world, b2Vec2 pos)
{
	b2BodyDef bodyDef;
	bodyDef.position = pos;
	bodyDef.type = b2_staticBody;
	b2Body* body = world.CreateBody(&bodyDef);

	b2CircleShape shape;
	shape.m_radius = (100/2) / SCALE;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	body->CreateFixture(&fixtureDef);
	std::shared_ptr<Ship> box = std::make_shared<Ship>(body);
	body->SetUserData(box.get());

	return (box);
}

Ship::Ship(b2Body *oself) : self(oself)
{
	Ressources *ressources = Ressources::getInstance();
	_sprite = std::make_shared<Sprite>(ressources->getTexture("ship_a"), 1, 100);
	_sprite->scale(5, 5);
	_sprite->setPosition(-150, -150);

	_rectal.setRadius(50);
	_rectal.setOutlineThickness(-1);
	_rectal.setFillColor(sf::Color::Transparent);
	_rectal.setOutlineColor(sf::Color::Red);
	_rectal.setPosition(0, 0);
}

bool Ship::dead()
{
	return (false);
}

void Ship::remove(b2World &world)
{
	world.DestroyBody(self);
}

void Ship::update(b2World &world, float timeStep)
{
	(void)world;
	_sprite->update(timeStep);
	b2Vec2 vect = self->GetPosition();
	setPosition(vect.x * SCALE - _sprite->getSize() / 2, -vect.y * SCALE - _sprite->getSize() / 2);
}

ElementType Ship::getType()
{
	return (ELM_OBSTACLE);
}

void Ship::beginContact(void *obj)
{
	(void)obj;
}

void Ship::endContact(void *obj)
{
	(void)obj;
}

void Ship::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(*_sprite, states);
	target.draw(_rectal, states);
}
