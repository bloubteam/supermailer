// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Shark.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/08/24 16:50:26 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/31 14:07:55 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Box.h>

std::shared_ptr<Box> Box::make(b2World& world, b2Vec2 pos)
{
	b2BodyDef bodyDef;
	bodyDef.position = pos;
	bodyDef.type = b2_staticBody;
	b2Body* body = world.CreateBody(&bodyDef);

	b2CircleShape shape;
	shape.m_radius = (100/2) / SCALE;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	body->CreateFixture(&fixtureDef);
	std::shared_ptr<Box> box = std::make_shared<Box>(body);
	body->SetUserData(box.get());

	return (box);
}

Box::Box(b2Body *oself) : self(oself)
{
	Ressources *ressources = Ressources::getInstance();
	_sprite = std::make_shared<Sprite>(ressources->getTexture("box_a"), 1, 100);
	_sprite->scale(5, 5);
	_sprite->setPosition(-150, -150);
}

bool Box::dead()
{
	return (false);
}

void Box::remove(b2World &world)
{
	world.DestroyBody(self);
}

void Box::update(b2World &world, float timeStep)
{
	(void)world;
	_sprite->update(timeStep);
	b2Vec2 vect = self->GetPosition();
	setPosition(vect.x * SCALE - _sprite->getSize() / 2, -vect.y * SCALE - _sprite->getSize() / 2);
}

ElementType Box::getType()
{
	return (ELM_OBSTACLE);
}

void Box::beginContact(void *obj)
{
	(void)obj;
}

void Box::endContact(void *obj)
{
	(void)obj;
}

void Box::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(*_sprite, states);
}
