#include    <Menu.h>

Menu::Menu()
{
	Ressources *ressources = Ressources::getInstance();

	if (!_img_texture.loadFromFile("./resources/boutonjouer.png"))
		throw ;
	_img_sprite.setTexture(_img_texture);
	_view = ressources->getWindow()->getDefaultView();
	_view.setSize(1920, 1080);
	_view.setCenter(_img_sprite.getLocalBounds().width / 2, _img_sprite.getLocalBounds().height / 2);

    _music = ressources->getMusic("main");
    _music->setVolume(100);
    _music->setLoop(true);
    _music->play();

	rectangle.setFillColor(sf::Color::Transparent);
    rectangle.setPosition(835, 755);
    rectangle.setSize(sf::Vector2f(400, 200));
    rectangle.setRotation(-5);
}

Menu::~Menu()
{
}

void                Menu::doLaunch()
{
    Ressources   *ressources = Ressources::getInstance();

    ressources->setStatut(STA_GAME);
}

void    Menu::event()
{
    Ressources          *ressources = Ressources::getInstance();
    sf::RenderWindow    *window;
    sf::Event           event;

    window = ressources->getWindow();
    while (window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            window->close();
    }
}

bool    Menu::isContains(const sf::Vector2f v1)
{
    if (rectangle.getGlobalBounds().contains(v1))
        return (true);
    return (false);
}

void     Menu::main()
{
	Ressources          *ressources = Ressources::getInstance();
    sf::RenderWindow    *window;


    event();
	window = ressources->getWindow();
	window->setView(_view);
	window->setMouseCursorVisible(true);
    window->clear(sf::Color::Black);

	sf::Vector2f v1=window->mapPixelToCoords(static_cast<sf::Vector2i>(sf::Mouse::getPosition(*window)));
	if (isContains(v1))
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			doLaunch();
	}
	if (ressources->getStatut() == STA_GAME)
        return;
	window->draw(_img_sprite);
	window->draw(rectangle);
    window->display();
}
