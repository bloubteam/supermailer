// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/21 01:42:51 by lcaminon          #+#    #+#             //
//   Updated: 2014/09/04 21:25:32 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Player.h>

std::shared_ptr<Player> Player::make(b2World &world)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(0, 10);
	bodyDef.type = b2_dynamicBody;
	b2Body* body = world.CreateBody(&bodyDef);

	b2CircleShape shape;
	shape.m_radius = (240/2) / SCALE;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	body->CreateFixture(&fixtureDef);
	std::shared_ptr<Player> player = std::make_shared<Player>(body);
	body->SetUserData(player.get());

	return (player);
}

Player::Player(b2Body *oself)
{
	Ressources *ressources = Ressources::getInstance();
	_sprites[PLS_RUN] = std::make_shared<Sprite>(ressources->getTexture("run"), 10, 300);
	_sprites[PLS_JUMP] = std::make_shared<Sprite>(ressources->getTexture("jump"), 10, 300);
	_sprites[PLS_FALL] = std::make_shared<Sprite>(ressources->getTexture("fall"));
	self = oself;
	_jump = 2;
	_state = PLS_RUN;
	_sprite = _sprites[_state];
	_alive = true;
	_jumpSound.setBuffer(*ressources->getSound("jump"));

	_rectal.setRadius(120);
	_rectal.setPosition(30, 30);
	_rectal.setOutlineThickness(-1);
	_rectal.setFillColor(sf::Color::Transparent);
	_rectal.setOutlineColor(sf::Color::Red);
}

bool Player::dead()
{
	return (!_alive);
}

void Player::update(b2World &world, float timeStep)
{
	(void)world;
	if (self->GetLinearVelocity().y == 0)
		_state = PLS_RUN;
	else
		_state = PLS_JUMP;
	_sprite = _sprites[_state];
	_sprites[_state]->update(timeStep);
	if (_jump == 0)
		self->SetLinearVelocity(b2Vec2(50, 0));
	b2Vec2 vect = self->GetPosition();
	setPosition(vect.x * SCALE - _sprite->getSize() / 2, -vect.y * SCALE - _sprite->getSize() / 2);
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(*_sprite, states);
	//target.draw(_rectal, states);
}

void Player::remove(b2World &world)
{
	(void)world;
}

void Player::beginContact(void *obj)
{
	if (obj)
	{
		if (static_cast<IElement*>(obj)->getType() == ELM_OBSTACLE)
			_alive = false;
	}
	else
	{
		_jump = 0;
	}
}

void Player::endContact(void *obj)
{
	if (obj)
	{
	}
	else
	{
	}
}

void Player::jump()
{
	if (_jump < 2)
	{
		++_jump;
		float impulse = self->GetMass() * 40 + self->GetMass() * 10 / _jump;
		self->ApplyLinearImpulse(b2Vec2(0,impulse), self->GetWorldCenter(), true);
		_jumpSound.play();
	}
}

ElementType Player::getType()
{
	return (ELM_PLAYER);
}
