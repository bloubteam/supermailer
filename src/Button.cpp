#include    <Menu.h>

Button::Button(std::string name, void (Menu::*_ptr)())
{
    Ressources          *ressources = Ressources::getInstance();

    ptr = _ptr;
    _button.setString(name);
    _button.setColor(sf::Color::White);
    _button.setFont(*ressources->getFont());
    _button.setCharacterSize(150);
}

bool    Button::isContains(const sf::Vector2f v1)
{
    if (_button.getGlobalBounds().contains(v1))
        return (true);
    return (false);
}

void    Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	(void)states;
	target.draw(_button);
}

void    Button::update(int height)
{
    Ressources          *ressources = Ressources::getInstance();
    sf::RenderWindow    *window;

	(void)height;
    window = ressources->getWindow();
	sf::View view = window->getView();
    _button.setPosition(view.getSize().x -_button.getGlobalBounds().width
						- 1400/ 2, height + 150 * 2); // <-- set position
    sf::Vector2f        v1=static_cast<sf::Vector2f>(sf::Mouse::getPosition(*window));
    if (isContains(v1))
        _button.setColor(sf::Color(255, 0, 51));
    else
        _button.setColor(sf::Color::White);
}

int		Button::height()
{
	return (_button.getGlobalBounds().height);
}
