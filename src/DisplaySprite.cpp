#include	"DisplaySprite.h"

DisplaySprite::DisplaySprite(std::string img, int x, int y, float a, int flag)
{
	Ressources *ressources = Ressources::getInstance();

	if (!_img_texture.loadFromFile(img))
		throw ;
	_img_sprite.setTexture(_img_texture);
	_view = ressources->getWindow()->getDefaultView();
	_view.setSize(x, y);
	_view.setCenter(_img_sprite.getLocalBounds().width / 2, _img_sprite.getLocalBounds().height / 2);
	if (flag == 1)
		fadeIn(_img_sprite, a);
}

DisplaySprite::~DisplaySprite()
{
}

void	DisplaySprite::fadeIn(sf::Sprite _img_sprite, float a)
{
	Ressources *ressources = Ressources::getInstance();
	sf::RenderWindow    *window;
	sf::Clock clock;
	sf::Time elapsed = clock.getElapsedTime();

    window = ressources->getWindow();
    window->setView(_view);
	window->clear();
	window->draw(_img_sprite);
	window->display();
	while (elapsed.asSeconds() < 3)
		elapsed = clock.getElapsedTime();
	while (a >= 0)
	{
		_img_sprite.setColor(sf::Color(255, 255, 255, a));
		//std::cout << "a vaut " << a << std::endl;
		a = a - 1.8f;
		window->clear();
		window->draw(_img_sprite);
		window->display();
	}
	clock.restart();
	while (elapsed.asSeconds() < 3)
		elapsed = clock.getElapsedTime();
}

void	DisplaySprite::Display(int mybool)
{
	Ressources *ressources = Ressources::getInstance();
	sf::RenderWindow	*window;

	window = ressources->getWindow();
	window->setView(_view);
	if (mybool == 1)
		window->setMouseCursorVisible(false);
	else
		window->setMouseCursorVisible(true);
	window->clear();
	window->draw(_img_sprite);
    window->display();
}
