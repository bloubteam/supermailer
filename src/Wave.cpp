// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Wave.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/08/23 14:22:45 by lcaminon          #+#    #+#             //
//   Updated: 2014/08/31 16:33:19 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Wave.h>

Wave::Wave() : _waves(3), _backWaves(2), _backIsles(2)
{
	Ressources   *ressources = Ressources::getInstance();

	_waves[0].second.setTexture(*ressources->getTexture("wave_a"), true);
	_waves[0].first = sf::Vector2f(10, 10);
	_waves[1].second.setTexture(*ressources->getTexture("wave_b"), true);
	_waves[1].first = sf::Vector2f(5, -10);
	_waves[2].second.setTexture(*ressources->getTexture("wave_c"), true);
	_waves[2].first = sf::Vector2f(-5, -5);

	_backWaves[0].first = sf::Vector2f(2, 2);
	_backWaves[0].second.setTexture(*ressources->getTexture("backw"), true);
	_backWaves[1].first = sf::Vector2f(-3, 3);
	_backWaves[1].second.setTexture(*ressources->getTexture("backw"), true);

	std::uniform_int_distribution<int> distribution(-600, 600);
	int dist = distribution(*ressources->getREngine());
	_backIsles[0].first = sf::Vector2f(dist, 0);
	_backIsles[0].second.setTexture(*ressources->getTexture("back_isle_a"), true);
	std::uniform_int_distribution<int> distancer(300, 900);
	dist = distancer(*ressources->getREngine());
	if (dist % 2)
		_backIsles[1].first = _backIsles[0].first + sf::Vector2f(dist, 0);
	else
		_backIsles[1].first = _backIsles[0].first - sf::Vector2f(dist, 0);
	_backIsles[1].second.setTexture(*ressources->getTexture("back_isle_b"), true);

	for (auto &isle : _backIsles)
		isle.second.setTextureRect(sf::IntRect(1, 1, isle.second.getTextureRect().width, isle.second.getTextureRect().height));

	_sky.setTexture(*ressources->getTexture("sky"), true);

	_isle.setTexture(*ressources->getTexture("isle"), true);
	_isle.setPosition(-1100, -_isle.getLocalBounds().height - 100);

	_backg.setTexture(*ressources->getTexture("backg"), true);

	_degrade.setTexture(*ressources->getTexture("degrade"), true);

	for (auto &wave : _waves)
		wave.second.setTextureRect(sf::IntRect(1, 1, wave.second.getLocalBounds().width, wave.second.getLocalBounds().height));
	_pos = 0;
}

void Wave::update(float timeStep, float pos)
{
	sf::Transform transform;
	float y;
	float speed;
	sf::IntRect rect;

	transform.rotate(130 * timeStep, 0, 0);

	_sky.setPosition(pos - _sky.getLocalBounds().width / 2, -_sky.getLocalBounds().height);
	_backg.setPosition(pos - _backg.getLocalBounds().width / 2, -_backg.getLocalBounds().height);
	for (auto &isle : _backIsles)
		isle.second.setPosition(pos + isle.first.x - isle.second.getLocalBounds().width / 2, _backg.getPosition().y - isle.second.getLocalBounds().height);
	_degrade.setPosition(_backg.getPosition());

	y = 0;
	for (auto &bwave : _backWaves)
	{
		bwave.first = transform.transformPoint(bwave.first);
		bwave.second.setPosition(sf::Vector2f(pos - bwave.second.getLocalBounds().width / 2, y - 500) + bwave.first);
		y -= 150;
	}
	speed = 300;
	rect = _backWaves[0].second.getTextureRect();
	rect.left = static_cast<int>(rect.left + speed * timeStep) % rect.width;
	_backWaves[0].second.setTextureRect(rect);
	speed = 100;
	rect = _backWaves[1].second.getTextureRect();
	rect.left = static_cast<int>(rect.left + speed * timeStep) % rect.width;
	_backWaves[1].second.setTextureRect(rect);

	y = -20;
	speed = 1500;
	for (auto &wave : _waves)
	{
		wave.first = transform.transformPoint(wave.first);
		wave.second.setPosition(sf::Vector2f(pos - wave.second.getLocalBounds().width / 2, -213 + y) + wave.first);
		y += 20;
		rect = wave.second.getTextureRect();
		rect.left = static_cast<int>(rect.left + speed * timeStep) % rect.width;
		wave.second.setTextureRect(rect);
	}
}

void Wave::rearDraw(sf::RenderWindow *window)
{
	window->draw(_sky);
	for (auto &isle : _backIsles)
		window->draw(isle.second);
	window->draw(_backg);
	for (auto &bwave : _backWaves)
		window->draw(bwave.second);
	window->draw(_degrade);
	window->draw(_waves[0].second);
	window->draw(_waves[1].second);
	window->draw(_isle);
}

void Wave::frontDraw(sf::RenderWindow *window)
{
	window->draw(_waves[2].second);
}
