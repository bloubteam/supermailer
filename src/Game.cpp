// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Game.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/20 22:12:39 by lcaminon          #+#    #+#             //
//   Updated: 2014/09/04 21:47:23 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <Game.h>

Game::Game()
	: _gravity(0, -80),
	  _world(_gravity)
{
	_window = Ressources::getInstance()->getWindow();
	_view = _window->getDefaultView();
	_view.setSize(1920, 1080);
	_player = Player::make(_world);
	_elements[reinterpret_cast<IElement*>(_player.get())] = _player;

	_dist = 150;
	_mapGen.seed(42);
	std::uniform_int_distribution<int> distribution(30, 300);
	std::uniform_int_distribution<int> type(0, 1);
	for (int i = 0; i < 10; ++i)
	{
		std::shared_ptr<IElement> elem;
		if (type(_mapGen))
			elem = Box::make(_world, b2Vec2(_dist, 9));
		else
			elem = Shark::make(_world, b2Vec2(_dist, 9));			
		_elements[elem.get()] = elem;
		_dist += distribution(_mapGen);
	}
	_waterDef.type = b2_staticBody;
	_waterDef.position.Set(0, 130/SCALE);
	_waterBody = _world.CreateBody(&_waterDef);
	_waterShape.SetAsBox(5000000000 / SCALE, 1/SCALE);
	_waterFixture.shape = &_waterShape;
	_waterFixture.density = 1;
	_waterBody->CreateFixture(&_waterFixture);
	_world.SetContactListener(&_contact);
}

Game::~Game()
{
}

void Game::event()
{
	sf::Event event;

	while (_window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			_window->close();
		else if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Escape)
			{
				_window->close();
			}
			else if (!_player->dead() && event.key.code == sf::Keyboard::Space)
			{
				_player->jump();
			}
		}
	}
}

void Game::main()
{
	sf::Clock clock;
	sf::Time timeStep;

	timeStep = clock.restart();
	_window->setMouseCursorVisible(false);
	while (_window->isOpen() && !_player->dead())
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			clock.restart();
			timeStep = sf::Time::Zero;
		}
		else
			timeStep = clock.restart();
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			timeStep = sf::seconds(0.001);
		event();
		_world.Step(timeStep.asSeconds(), 8, 3);
		b2Vec2 vect = _player->self->GetPosition();
		vect.x *= SCALE;
		vect.y *= -SCALE;
		vect.x += 500;
		_view.setCenter(vect.x, -540);
		_window->setView(_view);
		_window->clear(sf::Color::White);
		_wave.update(timeStep.asSeconds(), vect.x);
		_wave.rearDraw(_window);
		for (b2Body* bodyIterator = _world.GetBodyList(); bodyIterator != 0;)
		{
			b2Body *actBody = bodyIterator;
			bodyIterator = bodyIterator->GetNext();
			IElement *element = reinterpret_cast<IElement*>(actBody->GetUserData());

			if (element)
			{
				// detruire s'qui faut détruire et dessiner l'bousin
				if (element->dead() || (element->getType() != ELM_PLAYER && actBody->GetPosition().x < _player->self->GetPosition().x - 40))
				{
					// delete element (de façon memory-friendly => sharp-pointer négro !)
					element->remove(_world);
					_elements.erase(element);
					std::uniform_int_distribution<int> distribution(30, 300);
					std::uniform_int_distribution<int> type(0, 1);
					std::shared_ptr<IElement> elem;
					if (type(_mapGen))
						elem = Box::make(_world, b2Vec2(_dist, 9));
					else
						elem = Shark::make(_world, b2Vec2(_dist, 9));			
					_elements[elem.get()] = elem;
					_dist += distribution(_mapGen);
				}
				else
				{
					element->update(_world, timeStep.asSeconds());
					_window->draw(*element);
				}
			}
		}
		_window->draw(*_player);
		_wave.frontDraw(_window);
		_window->display();
	}
	Ressources *ressources = Ressources::getInstance();
	std::stringstream ss;
	ss << static_cast<int>(_player->self->GetPosition().x);
	sf::Text text(ss.str(), *ressources->getFont(), 90);
	text.setColor(sf::Color::Black);
	text.setPosition(1920/2 - text.getLocalBounds().width/2, 1080/2 - text.getLocalBounds().height/2);
	_view.setCenter(1920/2, 1080/2);
	_window->setView(_view);
	ressources->getMusic("main")->stop();
	sf::Sound dead(*ressources->getSound("dead"));
	dead.play();
	sf::Sprite lastScreen(*ressources->getTexture("highscore"));

	while (_window->isOpen())
	{
		_window->clear(sf::Color::White);
		event();
		_window->draw(lastScreen);
		_window->draw(text);
		_window->display();
	}
}
