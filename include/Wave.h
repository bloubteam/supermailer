/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Wave.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/23 13:55:31 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/24 17:27:06 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>

#ifndef WAVE
#define WAVE

class Wave
{
	float _pos;
	std::vector<std::pair<sf::Vector2f, sf::Sprite>> _waves;
	std::vector<std::pair<sf::Vector2f, sf::Sprite>> _backWaves;
	std::vector<std::pair<sf::Vector2f, sf::Sprite>> _backIsles;
	sf::Sprite _sky;
	sf::Sprite _isle;
	sf::Sprite _backg;
	sf::Sprite _degrade;

public:
	Wave();
	void rearDraw(sf::RenderWindow *window);
	void frontDraw(sf::RenderWindow *window);
	void update(float timeStep, float pos);
};

#endif
