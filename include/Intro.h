#ifndef INTRO_H
# define INTRO_H

#include	<common.h>
#include	<unistd.h>
#include	"DisplaySprite.h"

class Intro
{
public:
	Intro();
	~Intro();
	void main();
private:
	sf::Texture _intro_texture;
	sf::Sprite	_intro_sprite;
	sf::View	_view;
	sf::Texture _img_texture;
	sf::Sprite _img_sprite;

};

#endif // !INTRO_H
