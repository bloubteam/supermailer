//
// Ressources.hh for arbrePhilo in /home/lcaminon/Documents/projets/arbrePhilo
//
// Made by Loic Caminondo
// Email   <loic@caminondo.fr>
//
// Started on  Thu Sep 12 09:16:11 2013 Loic Caminondo
// Last update Sat Nov 16 17:38:01 2013 Loic Caminondo
//

#ifndef				RESSOURCES_HH
#define				RESSOURCES_HH

#include			<SFML/Graphics.hpp>
#include			<SFML/Audio.hpp>
#include			<vector>
#include			<string>
#include			<chrono>
#include			<regex>
#include			<fstream>
#include			<Singleton.hh>

enum				State
{
	STA_INTRO,
	STA_MENU,
	STA_GAME,
	STA_EDITOR
};

enum				Quality
  {
    QUA_LOW,
    QUA_MED,
    QUA_HIG
  };

enum				Language
{
    LAN_FR,
    LAN_EN,
    LAN_EO
};

class				Ressources : public Singleton<Ressources>
{
	sf::Font		_font;
	std::map<std::string, sf::Texture>	_texture;
	std::map<Language, std::map<std::string, std::string> > _texts;
	std::map<std::string, sf::SoundBuffer> _sounds;
	std::map<std::string, sf::Music> _musics;
	sf::RenderWindow		*_window;
	Quality			_quality;
	Language		_language;
	sf::Thread		_th_loading;
	bool  			_isLoading;
	bool			_loadingError;
	State			_statut;
	std::string		_res_dir;
	std::default_random_engine _rGenerator;

	void loading();
public:
	Ressources();
	~Ressources();
	void load(sf::RenderWindow *window);
	bool isLoading();
	bool isOk();
	sf::Font *getFont(std::string type = "default");
	sf::Texture *getTexture(std::string type);
	sf::RenderWindow *getWindow();
	sf::Music *getMusic(std::string type);
	sf::SoundBuffer *getSound(std::string type);
	std::string getText(std::string text);
	int getDef();
	State getStatut();
	void setStatut(State statut);
	std::default_random_engine *getREngine();
};
 
#endif
