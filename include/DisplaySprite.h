#ifndef DISPLAY_SPRITE
# define DISPLAY_SPRITE

#include <common.h>

class DisplaySprite
{
public:
	DisplaySprite();
	DisplaySprite(std::string, int x, int y, float a, int flag);
	~DisplaySprite();
	void Display(int mybool);
	void fadeIn(sf::Sprite _img_sprite, float a);

private:
	sf::Texture _img_texture;
	sf::Sprite	_img_sprite;
	sf::View	_view;
};

#endif
