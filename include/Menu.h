#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include        <Button.h>
#include        <common.h>

class Menu
{
	void                event();
    sf::Music           *_music;
	sf::View			_view;

	void doLaunch();
public:
    Menu();
	~Menu();
	void main();
private:
	sf::Texture _img_texture;
	sf::Sprite _img_sprite;
	sf::RectangleShape rectangle;
	sf::Vector2i mouse;
	bool isContains(const sf::Vector2f v1);
};

#endif // MENU_H_INCLUDED
