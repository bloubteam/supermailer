/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Game.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 22:11:34 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/29 19:23:19 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_H
#define GAME_H

#include <common.h>
#include <IElement.h>
#include <Player.h>
#include <Shark.h>
#include <Box.h>
#include <Ship.h>
#include <Wave.h>
#include <ContactListener.h>

class Game
{
	sf::RenderWindow *_window;
	b2Vec2 _gravity;
	b2World _world;
	sf::View _view;
	Wave _wave;
	ContactListener _contact;

	std::default_random_engine _mapGen;
	int _dist;

	//penser a virer la merde de dessous, sauf celui qui va bien
	b2BodyDef _waterDef;
	b2PolygonShape _waterShape;
	b2FixtureDef _waterFixture;
	b2Body *_waterBody;

	std::map<IElement*, std::shared_ptr<IElement>> _elements;
	std::shared_ptr<Player> _player;

	void event();
	void draw();

public:
	Game();
	~Game();
	void main();
};

#endif
