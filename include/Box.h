/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Shark.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/24 16:35:00 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/29 17:56:24 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOX
#define BOX

#include <common.h>
#include <IElement.h>
#include <Sprite.hh>

class Box : public IElement
{
	std::shared_ptr<Sprite> _sprite;

	sf::CircleShape _rectal;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	b2Body *self;

	static std::shared_ptr<Box> make(b2World& world, b2Vec2 pos);
	Box(b2Body *oself);

	bool dead();
	void remove(b2World &world);
	void update(b2World &world, float timeStep);
	ElementType getType();
	void beginContact(void *obj);
	void endContact(void *obj);
};

#endif
