/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Shark.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/24 16:35:00 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/27 23:43:14 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHARK
#define SHARK

#include <common.h>
#include <IElement.h>
#include <Sprite.hh>

class Shark : public IElement
{
	std::shared_ptr<Sprite> _sprite;

	/* sf::CircleShape _rectal; */

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	b2Body *self;

	static std::shared_ptr<Shark> make(b2World& world, b2Vec2 pos);
	Shark(b2Body *oself);

	bool dead();
	void remove(b2World &world);
	void update(b2World &world, float timeStep);
	ElementType getType();
	void beginContact(void *obj);
	void endContact(void *obj);
};

#endif
