/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/21 01:43:06 by lcaminon          #+#    #+#             */
/*   Updated: 2014/09/04 21:24:15 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER
#define PLAYER

#include <common.h>
#include <IElement.h>
#include <Sprite.hh>

enum PlayerState
{
	PLS_RUN,
	PLS_JUMP,
	PLS_FALL
};

class Player : public IElement
{
	std::map<PlayerState, std::shared_ptr<Sprite>> _sprites;
	std::shared_ptr<Sprite> _sprite;
	sf::Sound _jumpSound;
	PlayerState _state;
	int _jump;
	bool _alive;

	sf::CircleShape _rectal;

public:
	b2Body *self;

	static std::shared_ptr<Player> make(b2World& world);

	Player(b2Body *oself);
	bool dead();
	void remove(b2World &world);
	void update(b2World &world, float timeStep);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	ElementType getType();
	void beginContact(void *obj);
	void endContact(void *obj);
	void jump();
};

#endif
