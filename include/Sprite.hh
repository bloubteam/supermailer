// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   sprite.hh                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cplumas <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2014/06/22 20:58:15 by cplumas           #+#    #+#             //
//   Updated: 2014/08/24 05:19:57 by lcaminon         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef				SPRITE_HH
# define			SPRITE_HH

#include			<common.h>

class				Sprite : public sf::Drawable, public sf::Transformable
{
public:
	Sprite(sf::Texture *texture, float imgPSec = 9, int size = 100);
	void			update(float timeStep);
	float			getSize();

private:
	float			_time;
	float			_limit;
	sf::Sprite		_sprite;
	sf::Vector2i	_anim;
	int				_size;

	void			nextSprite();
	virtual void	draw(sf::RenderTarget&, sf::RenderStates states) const;
};

#endif
