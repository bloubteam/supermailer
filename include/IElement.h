/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IElement.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/21 01:21:05 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/24 18:32:17 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IELEMENT
#define IELEMENT

#include <common.h>

enum	ElementType
{
	ELM_PLAYER,
	ELM_OBSTACLE,
	ELM_BONUS
};

class IElement : public sf::Drawable, public sf::Transformable
{
public:
	virtual bool dead() = 0;
	virtual void remove(b2World &world) = 0;
	virtual void update(b2World &world, float timeStep) = 0;
	virtual ElementType getType() = 0;
	virtual void beginContact(void *obj) = 0;
	virtual void endContact(void *obj) = 0;
};

#endif
