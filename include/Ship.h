/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Shark.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcaminon <lcaminon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/24 16:35:00 by lcaminon          #+#    #+#             */
/*   Updated: 2014/08/29 19:22:05 by lcaminon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHIP
#define SHIP

#include <common.h>
#include <IElement.h>
#include <Sprite.hh>

class Ship : public IElement
{
	std::shared_ptr<Sprite> _sprite;

	sf::CircleShape _rectal;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	b2Body *self;

	static std::shared_ptr<Ship> make(b2World& world, b2Vec2 pos);
	Ship(b2Body *oself);

	bool dead();
	void remove(b2World &world);
	void update(b2World &world, float timeStep);
	ElementType getType();
	void beginContact(void *obj);
	void endContact(void *obj);
};

#endif
